#! /usr/bin/env python
"""
This is the module level docstring for `mymodule`.
"""

print("Hello from `mymodule`")

def func() -> None:
    print("You just ran the function called `func` from module `mymodule`")
    return

__all__ = ['default']
